#!/usr/bin/env make -f
# "Quiet Coins" by Ryan Pavlik
# Copyright 2020, Ryan Pavlik <ryan.pavlik@gmail.com>
# SPDX-License-Identifier: CC-BY-4.0

OUTFILES := blank-token.stl number-tokens.stl

OPENSCAD ?= openscad
PYTHON ?= python3

all: $(OUTFILES)
.PHONY: all

clean:
	-rm -f $(patsubst %,%.deps,$(OUTFILES))
.PHONY: clean

# Even remove committed files that are generated
realclean: clean
	-rm -f $(OUTFILES)
.PHONY: realclean

# Include deps files, if available.
-include $(wildcard *.deps)

# generate stl and canonicalize
$(OUTFILES) : %.stl : %.scad Makefile
	$(OPENSCAD) -o $@ -d $@.deps $<
	$(PYTHON) third-party/c14n_stl.py $@
